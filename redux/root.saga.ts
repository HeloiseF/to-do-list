import { all, fork } from "redux-saga/effects";
import watchUsersSagas from './users/saga/users.sagas';
import watchTasksSagas from "./taskList/taskList.sagas";


function* rootSaga() {
    yield all([fork(watchUsersSagas)]);
    yield all([fork(watchTasksSagas)]);
}

export default rootSaga;

