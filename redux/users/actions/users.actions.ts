import { User } from "../../../types/dbcontent.types";
import { UsersActionTypes } from "../users.types";
import { GetLoggedUserAction, SetLoggedUserAction, GetUsersAction, SetUsersAction } from "../users.types";

export const getLoggedUser = (userId: number): GetLoggedUserAction => ({
    type: UsersActionTypes.GET_LOGGED_USER,
    payload: userId
});

export const setLoggedUser = (userInfo: User): SetLoggedUserAction => ({
    type: UsersActionTypes.SET_LOGGED_USER,
    payload: userInfo
});


export const getUsers = (): GetUsersAction => ({
    type: UsersActionTypes.GET_USERS,
});

export const setUsers = (users: User[]): SetUsersAction => ({
    type: UsersActionTypes.SET_USERS,
    payload: users
});