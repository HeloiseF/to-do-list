import * as actions from '../actions/users.actions'
import { UsersActionTypes } from "../users.types";

test('create a getLoggedUser action', () => {
    const expectedAction = {
        type: UsersActionTypes.GET_LOGGED_USER,
        payload: 1
    }
    expect(actions.getLoggedUser(1)).toEqual(expectedAction)
})


