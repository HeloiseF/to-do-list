import { setLoggedUser } from "../actions/users.actions";
import usersReducer from "./users.reducer";

test('should return the initial state',()=>{
    const initialState = undefined;
    const action = {
        type:"",
        payload:[]
    };
    const result = usersReducer(initialState,action)
    expect(result).toEqual({
           "loggedUser": null,
           "users":  [],
         });
})


test('should handle setLoggedUser',()=>{
    const initialState = undefined;
    const fakeUser = {
        id: 1,
        userName: "Pierre",
        email: "Pierre@gmail.com",
        userPicture: "https://i.pravatar.cc/150?img=17"
    }
    const action = setLoggedUser(fakeUser);
    const result = usersReducer(initialState,action)
    expect(result).toEqual({
           "loggedUser": fakeUser,
           "users":  [],
         });
})