import { UsersActionTypes } from "../users.types";
import { User } from "../../../types/dbcontent.types";
import { SetLoggedUserAction, SetUsersAction } from '../users.types';


interface UserState {
    loggedUser: User,
    users: User[]
}

const initialState: UserState = {
    loggedUser: null,
    users: []
}

type Action = SetLoggedUserAction | SetUsersAction

const usersReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case UsersActionTypes.SET_LOGGED_USER:
            return {
                ...state,
                loggedUser: action.payload
            }
        case UsersActionTypes.SET_USERS:
            return {
                ...state,
                users: action.payload
            }
        default:
            return state;
    }
}

export default usersReducer;