import { User } from "../../types/dbcontent.types";
import { Action, ActionWithoutPayload } from "../../types/action.types";

export const UsersActionTypes = {
    GET_LOGGED_USER: 'GET_LOGGED_USER',
    GET_USERS: 'GET_USERS',
    SET_LOGGED_USER: 'SET_LOGGED_USER',
    SET_USERS: 'SET_USERS'
}

export type GetLoggedUserAction = Action<number>;
export type SetLoggedUserAction = Action<User>;
export type GetUsersAction = ActionWithoutPayload;
export type SetUsersAction = Action<User[]>;

