import { all, takeLatest, takeEvery, call, put } from "redux-saga/effects";

import { getLoggedUserRequest, getUsersRequest } from "../users.requests";
import { getLoggedUser, setLoggedUser, setUsers } from "../actions/users.actions";
import { UsersActionTypes, GetLoggedUserAction } from "../users.types";
import { User } from "../../../types/dbcontent.types";

export function* getLoggedUserSaga(action: GetLoggedUserAction) {
 
    const  result  = yield call(getLoggedUserRequest, action.payload);


    yield put(setLoggedUser(result.data));
}



function* getUsersSaga() {
    const { data } = yield call(getUsersRequest);
    yield put(setUsers(data));
}

export default function* watchUsersSagas() {
    yield all([takeLatest(UsersActionTypes.GET_LOGGED_USER, getLoggedUserSaga)]);
    yield all([takeLatest(UsersActionTypes.GET_USERS, getUsersSaga)]);
}



