
import { getLoggedUserRequest } from '../users.requests';
import { UsersActionTypes } from '../users.types';
import {getLoggedUserSaga} from './users.sagas'

import { setLoggedUser } from '../actions/users.actions';
import { call, put } from 'redux-saga/effects';
import * as requests from '../users.requests'
import { runSaga } from 'redux-saga';
    const result = {
        data:{
            id: 1,
            userName: "Pierre",
            email: "Pierre@gmail.com",
            userPicture: "https://i.pravatar.cc/150?img=17"
        }
 
    }
    const action ={
        type: UsersActionTypes.GET_LOGGED_USER,
        payload: 1
    }
test("test effects ",async()=>{

    const generator = getLoggedUserSaga(action);

    expect(generator.next().value).toEqual(call(getLoggedUserRequest, 1));
    expect(generator.next(result).value).toEqual(put(setLoggedUser(result.data)));

})


test("shoul call api  ",async()=>{
    // mock  request function 

    // jest.spyOn : mock function and watch calls 
    const requestLoggedUser = jest.spyOn(requests,'getLoggedUserRequest').mockImplementation((id) => Promise.resolve(result));
    const dispatched = [];
    let res = await requestLoggedUser();
    expect(requestLoggedUser).toHaveBeenCalledTimes(1);
    requestLoggedUser.mockRestore();
})


test("shoul call api and dispatch setLoggedUser action ",async()=>{
    // mock  request function 

    // jest.spyOn : mock function and watch calls 
    const requestLoggedUser = jest.spyOn(requests,'getLoggedUserRequest').mockImplementation((id) => Promise.resolve(result));
    const dispatched = [];

    // runSaga: Allows starting sagas outside the Redux middleware environment
    // dispatch for put effects
    await runSaga({dispatch : (act)=> dispatched.push(act)},getLoggedUserSaga,action )
    expect(requestLoggedUser).toHaveBeenCalledTimes(1);
    expect(dispatched[0]).toEqual(setLoggedUser(result.data));

    requestLoggedUser.mockRestore();

})