import axios from "axios";
import { User } from "../../types/dbcontent.types";

export const getLoggedUserRequest = async (id: number): Promise<User> => {
    return await axios.get(`${process.env.NEXT_PUBLIC_SERVER}/api/users/${id}`);
}

export const getUsersRequest = async (): Promise<User[]> => {
    return await axios.get(`${process.env.NEXT_PUBLIC_SERVER}/api/users`);
}