import axios from "axios";
import { Task } from "../../types/dbcontent.types";

export const getTasksByUserRequest = async (userId: number): Promise<Task[]> => {
    return await axios.get(`${process.env.NEXT_PUBLIC_SERVER}/api/users/${userId}/tasks`);
}

export const addTaskRequest = async (task: Task): Promise<void> => {
    return await axios.post(`${process.env.NEXT_PUBLIC_SERVER}/api/tasks`, task);
}

export const deleteTaskRequest = async (taskId: number): Promise<void> => {
    return await axios.delete(`${process.env.NEXT_PUBLIC_SERVER}/api/tasks/${taskId}`);
}