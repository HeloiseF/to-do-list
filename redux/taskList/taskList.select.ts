import { createSelector } from 'reselect';
import { StoreState } from '../root.reducer'

const selectTaskList = (state: StoreState) => state.taskList;

export const selectTasks = createSelector(
    [selectTaskList], taskList => taskList.tasks
);

export const selectFormTask = createSelector(
    [selectTaskList], taskList => taskList.formTask
);

export const selectSelectedTaskId = createSelector(
    [selectTaskList], taskList => taskList.selectedTaskId
)

export const selectTask = createSelector(
    [selectTasks, selectSelectedTaskId], (tasks, selectedTaskId) => {
        return tasks.find(task => task.id === selectedTaskId)
    }
)

export const selectIsCompleted = createSelector(
    [selectTasks, selectSelectedTaskId], (tasks, selectedTaskId) => {
        return tasks.find(task => task.id === selectedTaskId).isCompleted
    }
)
