import { Task } from "../../types/dbcontent.types";
import { Action, ActionWithoutPayload } from "../../types/action.types";
import { Update } from "./taskList.utils";

export const TaskActionTypes = {
    GET_TASKS: 'GET_TASKS',
    SET_TASKS: 'SET_TASKS',

    ADD_TASK: 'ADD_TASK',
    UPDATE_TASK: 'UPDATE_TASK',
    DELETE_TASK: 'DELETE_TASK',

    ADD_SELECTED_ID: 'ADD_SELECTED_ID',
    ADD_NEW_ID: 'ADD_NEW_ID',
    SET_FORM_TASK: 'SET_FORM_TASK',


    TOGGLE_IS_COMPLETED: "TOGGLE_IS_COMPLETED",
    TOGGLE_DISPLAY: "TOGGLE_DISPLAY"
};



export type GetTasksAction = Action<number>;
export type SetTasksAction = Action<Task[]>;

export type AddTaskAction = Action<{ task: Task, userId: number }>;
export type UpdateTaskAction = Action<{ task: Task, userId: number }>;
export type DeleteTaskAction = Action<{ selectedTaskId: number, userId: number }>;

export type AddSelectedIdAction = Action<number>;
export type AddNewIdAction = ActionWithoutPayload;
export type SetFormTaskAction = Action<Update>;

export type ToggleIsCompletedAction = Action<number>;
export type ToggleIsDisplayAction = Action<string>;

