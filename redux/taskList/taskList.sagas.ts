import { AddTaskAction, DeleteTaskAction, GetTasksAction, TaskActionTypes, UpdateTaskAction } from "./taskList.types";
import { getTasksByUserRequest, addTaskRequest, deleteTaskRequest } from './tasks.requests';
import { setTasks, getTasks } from "./taskList.action";
import { takeLatest, all, call, put } from "redux-saga/effects";
import { Task } from "../../types/dbcontent.types";

function* getTasksByUserSaga(action: GetTasksAction) {
    const userId = action.payload;
    const { data }: { data: Task[] } = yield call(getTasksByUserRequest, userId);
    yield put(setTasks(data));
}

function* addTaskSaga(action: AddTaskAction) {
    const task = action.payload.task;
    const userId = action.payload.userId;
    yield call(addTaskRequest, task);
    yield put(getTasks(userId));
}

function* updateTaskSaga(action: UpdateTaskAction) {
    const task = action.payload.task;
    const userId = action.payload.userId;
    yield call(addTaskRequest, task);
    yield put(getTasks(userId));
}

function* deleteTaskSaga(action: DeleteTaskAction) {
    const selectedTaskId = action.payload.selectedTaskId;
    const userId = action.payload.userId;
    yield call(deleteTaskRequest, selectedTaskId);
    yield put(getTasks(userId));
}


export default function* watchTasksSagas() {
    yield all([takeLatest(TaskActionTypes.ADD_TASK, addTaskSaga)]);
    yield all([takeLatest(TaskActionTypes.GET_TASKS, getTasksByUserSaga)]);
    yield all([takeLatest(TaskActionTypes.UPDATE_TASK, updateTaskSaga)]);
    yield all([takeLatest(TaskActionTypes.DELETE_TASK, deleteTaskSaga)]);
}

