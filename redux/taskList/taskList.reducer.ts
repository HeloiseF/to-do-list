import { Task } from "../../types/dbcontent.types";
import { TaskActionTypes } from "./taskList.types";
import { setForm, toggleCompletedTask } from "./taskList.utils";
import { SetTasksAction, AddSelectedIdAction, AddNewIdAction, SetFormTaskAction, ToggleIsCompletedAction, ToggleIsDisplayAction } from './taskList.types';

export interface TaskListState {
    selectedTaskId: number;
    formTask: {
        displayUsers: boolean,
        displayCalendar: boolean,
        task: Task
    },
    tasks: Task[];
}


const initialState: TaskListState = {
    selectedTaskId: null,
    formTask: {
        displayUsers: false,
        displayCalendar: false,
        task: {
            id: null,
            description: '',
            endDate: null,
            isCompleted: false,
            title: '',
            assignee: {
                userName: '',
                userPicture: '',
                id: null,
            }
        }
    },
    tasks: []
};



type Action = SetTasksAction | AddSelectedIdAction | SetFormTaskAction | ToggleIsCompletedAction | ToggleIsDisplayAction | AddNewIdAction;


const taskListReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case TaskActionTypes.SET_TASKS:
            return {
                ...state,
                selectedTaskId: initialState.selectedTaskId,
                formTask: initialState.formTask,
                tasks: (action as SetTasksAction).payload
            }
        case TaskActionTypes.ADD_SELECTED_ID:
            return {
                ...state,
                selectedTaskId: (action as AddSelectedIdAction).payload
            }
        case TaskActionTypes.ADD_NEW_ID:
            return {
                ...state,
                formTask: { ...state.formTask, task: { ...state.formTask.task, id: 0 } },
                selectedTaskId: 0
            }
        case TaskActionTypes.SET_FORM_TASK:
            return {
                ...state,
                formTask: setForm(state.formTask, (action as SetFormTaskAction).payload)
            }
        case TaskActionTypes.TOGGLE_IS_COMPLETED:
            return {
                ...state,
                formTask: {
                    ...state.formTask,
                    task: { ...state.formTask.task, isCompleted: !state.formTask.task.isCompleted }
                },
                tasks: toggleCompletedTask(state.tasks, (action as ToggleIsCompletedAction).payload)
            }
        case TaskActionTypes.TOGGLE_DISPLAY:
            return {
                ...state,
                formTask: { ...state.formTask, [(action as ToggleIsDisplayAction).payload]: !state.formTask[(action as ToggleIsDisplayAction).payload] }
            }
        default:
            return state;
    }
}
export default taskListReducer;