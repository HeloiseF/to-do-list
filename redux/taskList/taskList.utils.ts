import { Task } from "../../types/dbcontent.types"

export interface Form {
    displayUsers: boolean,
    displayCalendar: boolean,
    task: Task
}
export interface Update {
    id?: number;
    description?: string,
    endDate?: Date,
    isCompleted?: boolean,
    title?: string,
    assignee?: {
        userName: string,
        userPicture: string,
        id: number,
    }
}


export const setForm = (form: Form, update: Update) => {
    return {
        ...form,
        task: { ...form.task, ...update }
    }
}

export const toggleCompletedTask = (tasks: Task[], taskId: number) => {
    const tasksArrayCopy = [...tasks];
    const index = tasksArrayCopy.findIndex((task) => task.id === taskId);
    const isCompleted = tasksArrayCopy[index].isCompleted;
    tasksArrayCopy[index].isCompleted = !isCompleted;
    return tasksArrayCopy;
}