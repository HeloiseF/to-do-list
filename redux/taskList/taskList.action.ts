import { Task } from "../../types/dbcontent.types";
import { TaskActionTypes } from './taskList.types'
import { AddTaskAction, GetTasksAction, SetFormTaskAction, SetTasksAction, UpdateTaskAction, DeleteTaskAction, AddSelectedIdAction, AddNewIdAction, ToggleIsCompletedAction, ToggleIsDisplayAction } from "./taskList.types";
import { Update } from "./taskList.utils";


export const getTasks = (userId: number): GetTasksAction => ({
    type: TaskActionTypes.GET_TASKS,
    payload: userId
});

export const setTasks = (tasks: Task[]): SetTasksAction => ({
    type: TaskActionTypes.SET_TASKS,
    payload: tasks
});

export const addTask = ({ task, userId }: { task: Task, userId: number }): AddTaskAction => ({
    type: TaskActionTypes.ADD_TASK,
    payload: { task, userId }
})

export const updateTask = ({ task, userId }: { task: Task, userId: number }): UpdateTaskAction => ({
    type: TaskActionTypes.UPDATE_TASK,
    payload: { task, userId }
});

export const deleteTask = ({ selectedTaskId, userId }: { selectedTaskId: number, userId: number }): DeleteTaskAction => ({
    type: TaskActionTypes.DELETE_TASK,
    payload: { selectedTaskId, userId }
});

export const addSelectedId = (taskId: number): AddSelectedIdAction => ({
    type: TaskActionTypes.ADD_SELECTED_ID,
    payload: taskId
});

export const addNewId = (): AddNewIdAction => ({
    type: TaskActionTypes.ADD_NEW_ID,
});

export const setFormTask = (update: Update): SetFormTaskAction => ({
    type: TaskActionTypes.SET_FORM_TASK,
    payload: update
});

export const toggleIsCompleted = (taskId: number): ToggleIsCompletedAction => ({
    type: TaskActionTypes.TOGGLE_IS_COMPLETED,
    payload: taskId
});

export const toggleDisplay = (key: string): ToggleIsDisplayAction => ({
    type: TaskActionTypes.TOGGLE_DISPLAY,
    payload: key
});