import { combineReducers } from "redux";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import taskListReducer from "./taskList/taskList.reducer";
import usersReducer from "./users/reducer/users.reducer";
import { User } from "../types/dbcontent.types";
import { TaskListState } from './taskList/taskList.reducer'

const persistConfig = {
    key: 'root',
    storage,
    whiteList: ['taskList', 'users'],
};

const rootReducer = combineReducers({
    taskList: taskListReducer,
    users: usersReducer
})

export type StoreState = {
    taskList: TaskListState;
    users: {
        loggedUser: User,
        users: User[]
    }
}

export default persistReducer(persistConfig, rootReducer);