import { addNewId, setFormTask } from "../../redux/taskList/taskList.action";
import { connect } from "react-redux";
import { selectFormTask } from "../../redux/taskList/taskList.select";
import { Dispatch } from "redux";
import { Form, Update } from "../../redux/taskList/taskList.utils";
import { StoreState } from "../../redux/root.reducer";


interface FormTaskListProps {
    addNewId: typeof addNewId,
    setFormTask: typeof setFormTask,
    formTask: Form
}

const FormTaskList = ({ addNewId, setFormTask, formTask }: FormTaskListProps): JSX.Element => {

    const handleSubmit = (e) => {
        e.preventDefault();
        addNewId();

    }
    return (
        <form onSubmit={handleSubmit} >
            <input type="text" placeholder="+ Ajouter une tache..." name='title' value={formTask.task.title} onChange={(e) => setFormTask({ title: e.target.value })} />
        </form>
    )
}

const mapStateToProps = (state: StoreState) => ({
    formTask: selectFormTask(state),
})

const mapStateToDispatch = (dispatch: Dispatch) => ({
    addNewId: () => dispatch(addNewId()),
    setFormTask: (update: Update) => dispatch(setFormTask(update))
})

export default connect(mapStateToProps, mapStateToDispatch)(FormTaskList);