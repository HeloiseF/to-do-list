import STaskListItem from '../../styles/TaskListItem.module';
import {
  addSelectedId,
  setFormTask,
} from '../../redux/taskList/taskList.action';
import { useDispatch, useSelector } from 'react-redux';
import Image from 'next/image';
import { StoreState } from '../../redux/root.reducer';
import { Task } from '../../types/dbcontent.types';

interface TaskListItemProps {
  task: Task;
}

const TaskListItem = ({ task }: TaskListItemProps): JSX.Element => {
  const dispatch = useDispatch();
  const selectedTaskId = useSelector(
    (state: StoreState) => state.taskList.selectedTaskId
  );
  const { id, isCompleted, assignee, endDate, title } = task;
  const handleClick = () => {
    dispatch(setFormTask(task));
    dispatch(addSelectedId(id));
  };

  return (
    <STaskListItem
      isCompleted={isCompleted === true}
      isSelected={selectedTaskId === task.id ? true : false}
      userPicture={assignee.userPicture}
    >
      <button onClick={handleClick}>
        <span className={isCompleted === true ? 'cercle completed' : 'cercle'}>
          {isCompleted === true && (
            <Image
              height='40px'
              width='40px'
              src='/images/completed.png'
              alt='icone terminé'
            />
          )}
        </span>
        <div className='left'>
          <h4>{title}</h4>
          <span className='userName'>{assignee.userName}</span>
        </div>

        <span className='endDate'>
          {new Date(endDate).toLocaleString('fr-FR', {
            day: 'numeric',
            month: 'long',
            year: 'numeric',
          })}
        </span>
      </button>
    </STaskListItem>
  );
};

export default TaskListItem;
