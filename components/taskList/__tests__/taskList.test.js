import '@testing-library/jest-dom/extend-expect';
import { Provider, ReactReduxContext } from 'react-redux';
import { render, screen, waitFor } from "../../../utils/test-utils";
import TaskListItem from '../taskListItem';

const fakeDate = new Date();
    const fakeTask = {
        id: 1,
        title:"Title",
        description: "test description",
        endDate : fakeDate,
        isCompleted:false,
        assignee:{
            id:1,
            userName:"Heloise ferla",
            userPicture: "https://i.pravatar.cc/150?img=17"
        }

    }


test("render a TaskList component with a title, the username and the date",async ()=> {
    const {rerender, container}=render(<TaskListItem task = {fakeTask}/>)

    expect(screen.getByText("Heloise ferla")).toBeInTheDocument();
    expect(screen.getByText("Title")).toBeInTheDocument();
    expect(screen.getByText(new RegExp(fakeDate.toLocaleString('fr-FR', {
        day: 'numeric',
        month: 'long',
        year: 'numeric',
      }), "i"))).toBeInTheDocument();


      expect(screen.queryAllByAltText("icone terminé")).toHaveLength(0);
      expect(container.getElementsByClassName('completed').length).toBe(0)

    rerender(<TaskListItem task={{...fakeTask,isCompleted:true}}/>)
    expect(screen.queryAllByAltText("icone terminé")).toHaveLength(1);
    expect(container.getElementsByClassName('completed').length).toBe(1)
 
})