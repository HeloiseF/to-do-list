import STaskListContainer from "../../styles/TaskListContainer.module";
import TaskList from "./taskList";
import FormTaskList from "./formTaskList";


const TaskListContainer = (): JSX.Element => {
    return (
        <STaskListContainer>
            <h3>Toutes les taches</h3>
            <FormTaskList />
            <TaskList completedTasks={false} />
            <TaskList completedTasks={true} />
        </STaskListContainer>
    )
}

export default TaskListContainer;
