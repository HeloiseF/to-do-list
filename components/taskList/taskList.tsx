import STaskList from '../../styles/TaskList.module';
import { connect } from 'react-redux';
import TaskListItem from './taskListItem';
import { selectTasks } from '../../redux/taskList/taskList.select';
import { StoreState } from '../../redux/root.reducer';
import { Task, User } from '../../types/dbcontent.types';
import { useDrop } from 'react-dnd';
import { ItemTypes } from '../../types/dnd';
import { Dispatch } from 'redux';
import { updateTask } from '../../redux/taskList/taskList.action';

interface TasklistProps {
  tasks: Task[];
  updateTask: typeof updateTask;
  loggedUser: User;
  completedTasks: boolean;
}

const TaskList = ({
  tasks,
  updateTask,
  loggedUser,
  completedTasks,
}: TasklistProps): JSX.Element => {
  const total = tasks.reduce(
    (prev, task) => (task.isCompleted === completedTasks ? prev + 1 : prev),
    0
  );

  const [{ isOver }, dropRef] = useDrop({
    accept: completedTasks
      ? ItemTypes.IN_PROGRESS_TASK
      : ItemTypes.COMPLETED_TASK,
    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
    }),
    drop: (item: Task) =>
      updateTask({
        task: { ...item, isCompleted: !item.isCompleted },
        userId: loggedUser.id,
      }),
  });

  return (
    <STaskList>
      <div className={completedTasks ? 'completedTasks' : 'inProgressTasks'}>
        <div className='title'>
          <span>{completedTasks ? 'Taches terminées' : 'Taches en cours'}</span>
          <span>{total}</span>
        </div>

        <ul ref={dropRef}>
          {isOver && (
            <div className='isOver'>
              {completedTasks
                ? 'Retire la tache des tâches terminées !!'
                : 'Ajoute la tache aux tâches terminées !!'}{' '}
            </div>
          )}
          {tasks
            .filter((task) =>
              completedTasks
                ? task.isCompleted === true
                : task.isCompleted !== true
            )
            .map((task) => (
              <TaskListItem key={task.id} task={task} />
            ))}
        </ul>
      </div>
    </STaskList>
  );
};

const mapStateToProps = (state: StoreState) => ({
  tasks: selectTasks(state),
  loggedUser: state.users.loggedUser,
});

const mapToDispatch = (dispatch: Dispatch) => ({
  updateTask: ({ task, userId }: { task: Task; userId: number }) =>
    dispatch(updateTask({ task, userId })),
});

export default connect(mapStateToProps, mapToDispatch)(TaskList);
