import Image from 'next/image';
import SHeader from '../styles/Header.module';
import logo from '../public/images/Todoist_logo.png';
import { useSession } from 'next-auth/react';
import { signOut } from "next-auth/react";

const Header = (): JSX.Element => {
    const session = useSession();

    const handleClick = () => {
        signOut({
            callbackUrl: `${window.location.origin}`
        });
    }

    return (
        <SHeader>
            <div className='logo'>
                <Image layout="fill" objectFit="contain" src={logo} alt='logo' />
            </div>
            {session.data !== null &&
                <button onClick={handleClick}>Se déconnecter</button>
            }
        </SHeader>
    )
}

export default Header;