import { selectSelectedTaskId } from '../../redux/taskList/taskList.select';
import { Dispatch } from 'redux';
import { StoreState } from '../../redux/root.reducer';
import { toggleIsCompleted } from '../../redux/taskList/taskList.action';
import Image from 'next/image';
import checkIcon from '../../public/images/check.png';
import { connect, useDispatch, useSelector } from 'react-redux';
import { isConstructorDeclaration } from 'typescript';

const CompletedButton = (): JSX.Element => {
  const dispatch = useDispatch();

  const selectedTaskId = useSelector(
    (state: StoreState) => state.taskList.selectedTaskId
  );

  const isCompleted = useSelector((state: StoreState) => {
    state.taskList.tasks.find((task) => task.id === selectedTaskId).isCompleted;
    return state.taskList.tasks.find((task) => task.id === selectedTaskId)
      .isCompleted;
  });

  const handleCompletedButton = () => {
    dispatch(toggleIsCompleted(selectedTaskId));
  };

  return (
    <button className='completed' onClick={handleCompletedButton}>
      {isCompleted === true ? (
        <span>MARQU&#xC9; COMME NON TERMIN&#xC9;</span>
      ) : (
        <>
          <Image
            height='18px'
            width='18px'
            src={'/images/check.png'}
            alt='icone terminé'
          />
          <span>MARQU&#xC9; COMME TERMIN&#xC9;</span>
        </>
      )}
    </button>
  );
};

export default CompletedButton;
