import STaskDetail from '../../styles/TaskDetail.module';
import {
  selectTask,
  selectSelectedTaskId,
  selectFormTask,
} from '../../redux/taskList/taskList.select';
import {
  setFormTask,
  addTask,
  updateTask,
  deleteTask,
  toggleIsCompleted,
  toggleDisplay,
  getTasks,
} from '../../redux/taskList/taskList.action';
import { connect } from 'react-redux';
import Image from 'next/image';
import clockIcon from '../../public/images/clockIcon.png';
import createIcon from '../../public/images/icons8-create-50.png';
import UserList from '../userList/userList';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CompletedButton from './completedButton';
import { getUsers } from '../../redux/users/actions/users.actions';
import { StoreState } from '../../redux/root.reducer';
import { Dispatch } from 'redux';
import { Task, User } from '../../types/dbcontent.types';
import { Form } from '../../redux/taskList/taskList.utils';

interface TaskDetailProps {
  updateTask: typeof updateTask;
  selectedTask: Task;
  selectedTaskId: number;
  formTask: Form;
  setFormTask: typeof setFormTask;
  addTask: typeof addTask;
  deleteTask: typeof deleteTask;
  toggleDisplay: typeof toggleDisplay;
  loggedUser: User;
  getUsers: typeof getUsers;
}

const TaskDetail = ({
  updateTask,
  selectedTask,
  selectedTaskId,
  formTask,
  setFormTask,
  addTask,
  deleteTask,
  toggleDisplay,
  loggedUser,
  getUsers,
}: TaskDetailProps): JSX.Element => {
  const { task } = formTask;
  const handleChange = (e) => {
    const { value, name } = e.target;
    setFormTask({ [name]: value });
  };

  const handleClickUsers = () => {
    toggleDisplay('displayUsers');
    getUsers();
  };

  const handleDelete = () => {
    deleteTask({ selectedTaskId, userId: loggedUser.id });
  };

  const handleClick = () => {
    if (task.assignee.id === null) {
      toast.error("La tache n'est pas attribuée", {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else if (task.endDate === null) {
      toast.error("La tache ne possède pas d'échéance", {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else if (task.description === '') {
      toast.error('La tache ne possède pas de description', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else if (task.title === '') {
      toast.error('La tache ne possède pas de titre', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else if (selectedTask) {
      updateTask({ task, userId: loggedUser.id });
    } else {
      addTask({ task, userId: loggedUser.id });
    }
  };

  return (
    <STaskDetail userPicture={task.assignee.userPicture}>
      <ToastContainer />
      {selectedTaskId !== null && (
        <>
          <div className='top'>
            <input
              type='text'
              value={task.title}
              name='title'
              onChange={handleChange}
            />
            {selectedTask && <CompletedButton />}
          </div>
          <div className='UserEndDate'>
            <button
              className={task.assignee.userName ? 'user task' : 'user'}
              onClick={handleClickUsers}
            >
              {task.assignee.userName ? <span></span> : <span>+</span>}
              {task.assignee.userName ? (
                <span>{task.assignee.userName}</span>
              ) : (
                <span>ATTRIBUER &Agrave;</span>
              )}
            </button>
            {formTask.displayUsers === true && <UserList />}
            <button
              className='endDate'
              onClick={() => toggleDisplay('displayCalendar')}
            >
              <div>
                <Image
                  height='28px'
                  width='28px'
                  src={clockIcon}
                  alt='icone horloge'
                />
              </div>
              {!task.endDate ? (
                <span>&Eacute;CH&Eacute;ANCE</span>
              ) : (
                <span>
                  {new Date(task.endDate).toLocaleString('fr-FR', {
                    day: 'numeric',
                    month: 'long',
                    year: 'numeric',
                  })}
                </span>
              )}
            </button>
            {formTask.displayCalendar && (
              <Calendar
                value={task.endDate ? new Date(task.endDate) : new Date()}
                onChange={(date: Date) => {
                  setFormTask({ endDate: new Date(date) });
                  toggleDisplay('displayCalendar');
                }}
              />
            )}
          </div>
          <div className='description'>
            <div className='container'>
              <Image
                src={createIcon}
                alt='icone description'
                height={24}
                width={24}
                objectFit='contain'
              />
              <span className='description'>Description</span>
            </div>
            <textarea
              placeholder='Ajouter une description ...'
              value={task.description}
              name='description'
              onChange={handleChange}
            />

            <div className='buttonContainer'>
              <button onClick={handleDelete}>{`SUPPRIMER`}</button>
              <button onClick={handleClick}>
                {selectedTask ? `MODIFIER` : `AJOUTER`}
              </button>
            </div>
          </div>
        </>
      )}
    </STaskDetail>
  );
};

const mapStateToProps = (state: StoreState) => ({
  selectedTask: selectTask(state),
  selectedTaskId: selectSelectedTaskId(state),
  formTask: selectFormTask(state),
  loggedUser: state.users.loggedUser,
  tasks: state.taskList.tasks,
});
const mapToDispatch = (dispatch: Dispatch) => ({
  setFormTask: (update: Task) => dispatch(setFormTask(update)),
  addTask: ({ task, userId }: { task: Task; userId: number }) =>
    dispatch(addTask({ task, userId })),
  updateTask: ({ task, userId }: { task: Task; userId: number }) =>
    dispatch(updateTask({ task, userId })),
  deleteTask: ({
    selectedTaskId,
    userId,
  }: {
    selectedTaskId: number;
    userId: number;
  }) => dispatch(deleteTask({ selectedTaskId, userId })),
  toggleIsCompleted: (taskId: number) => dispatch(toggleIsCompleted(taskId)),
  toggleDisplay: (key: string) => dispatch(toggleDisplay(key)),
  getUsers: () => dispatch(getUsers()),
  getTasks: (userId: number) => dispatch(getTasks(userId)),
});

export default connect(mapStateToProps, mapToDispatch)(TaskDetail);
