
import CompletedButton from '../completedButton' ; 
import * as redux from 'react-redux'
import { render, screen, fireEvent } from '../../../utils/test-utils';
import '@testing-library/jest-dom'


const state = {

    taskList:{
        selectedTaskId:1,
        formTask:{
            displayUsers:false,
            displayCalendar:false,
            task:{
                id:1,
                description:"sf",
                endDate:"2022-04-01T00:00:00.000Z",
                isCompleted:false,
                title:"efef",
                assignee:{
                    id:1,
                    userName:"Philippe Armand",
                    userPicture:"https://i.pravatar.cc/150?img=17"
                }
            }
        },
        tasks:[
            {
                id:1,
                description:"sf",
                endDate:"2022-04-01T00:00:00.000Z",
                isCompleted:false,
                title:"efef",
                assignee:{
                    id:1,
                    userName:"Philippe Armand",
                    userPicture:"https://i.pravatar.cc/150?img=17"
                }
            },
            {
                id:2,
                description:"sf",
                endDate:"2022-04-01T00:00:00.000Z",
                isCompleted:true,
                title:"efef",
                assignee:{
                    id:1,
                    userName:"Philippe Armand",
                    userPicture:"https://i.pravatar.cc/150?img=17"
                }
            }
        ],
    },
        
  };




test('render completed button',()=> {
    const {container}=render(<CompletedButton/>,
    { initialState : state})

    expect(container.getElementsByClassName('completed').length).toBe(1)

    expect(screen.getByText(/MARQUÉ COMME TERMINÉ/i )).toBeInTheDocument();
    // const test = {...state,taskList:{...state.taskList,
    //     tasks:state.taskList.tasks.map((task)=> task.id === state.taskList.selectedTaskId ? {...task,isCompleted:true} : task)}}
    //     rerender(<CompletedButton/>,{ initialState :  test})
    //     expect(screen.getByText(/MARQUÉ COMME TERMINÉ/i )).toBeInTheDocument();
    fireEvent.click(screen.getByRole('button'))
    const button = screen.queryByRole('button')
    expect(screen.getByText(/MARQUÉ COMME NON TERMINÉ/i )).toBeInTheDocument();

})