import Image from "next/image";
import { SUserListItem } from "../../styles/UserList.module";
import { connect } from "react-redux";
import { selectFormTask } from "../../redux/taskList/taskList.select";
import { setFormTask, toggleDisplay } from "../../redux/taskList/taskList.action";
import { StoreState } from "../../redux/root.reducer";
import { Dispatch } from "redux";
import { Form, Update } from "../../redux/taskList/taskList.utils";
import { User } from "../../types/dbcontent.types";



interface UserListItemProps {
    user: User,
    formTask: Form,
    setFormTask: typeof setFormTask,
    toggleDisplay: typeof toggleDisplay
}

const UserListItem = ({ user, formTask, setFormTask, toggleDisplay }: UserListItemProps): JSX.Element => {


    const handleClick = () => {
        setFormTask({ assignee: { userName: user.userName, userPicture: user.userPicture, id: user.id } })
        toggleDisplay('displayUsers')
    }
    return (

        <SUserListItem isSelected={(formTask.task && formTask.task.assignee.id === user.id) ? true : false}>
            <button onClick={handleClick}>
                <Image className='picture' height={36} width={36} src={user.userPicture} alt={user.userName} />
                <div>
                    <span>{user.userName}</span>
                    <span>{user.email}</span>
                </div>
            </button>
        </SUserListItem>

    )
}

const mapStateToProps = (state: StoreState) => ({
    formTask: selectFormTask(state)

})
const mapToDispatch = (dispatch: Dispatch) => ({
    setFormTask: (update: Update) => dispatch(setFormTask(update)),
    toggleDisplay: (key: string) => dispatch(toggleDisplay(key))
})



export default connect(mapStateToProps, mapToDispatch)(UserListItem);