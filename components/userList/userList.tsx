import { SUserList } from "../../styles/UserList.module";
import UserListItem from "./userListItem";
import { connect } from "react-redux";
import { User } from "../../types/dbcontent.types";
import { StoreState } from "../../redux/root.reducer";


const UserList = ({ users }: { users: User[] }): JSX.Element => {
    return (
        <SUserList>
            {
                users.map((user) =>
                    <UserListItem key={user.id} user={user} />
                )
            }
        </SUserList>
    )
}


const mapStateToProps = (state: StoreState) => ({
    users: state.users.users
})

export default connect(mapStateToProps)(UserList);