// this is just a fake module to simulate interacting with a server

// simulate the network request time...
const sleep = time =>
  new Promise(resolve => {
    setTimeout(resolve, time)
  })

   async function fetchLoggedUser(id) {
    await sleep(1000)
    return {data: {
        id: id,
        userName: "Pierre",
        email: "Pierre@gmail.com",
        userPicture: "https://i.pravatar.cc/150?img=17"
    }}
  }
  export const api = {
    fetchLoggedUser,
  }