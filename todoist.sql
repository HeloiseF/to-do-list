--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.task (
    id integer NOT NULL,
    end_date date NOT NULL,
    description text NOT NULL,
    is_completed boolean DEFAULT false NOT NULL,
    title character varying(255) NOT NULL,
    user_id integer
);


ALTER TABLE public.task OWNER TO postgres;

--
-- Name: task_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.task ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    user_name character varying(255) NOT NULL,
    image_url character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password text
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.task (id, end_date, description, is_completed, title, user_id) FROM stdin;
192	2022-04-13	dzqdzq	t	5	1
190	2022-04-07	zqdzdqdq	t	2	1
194	2022-04-14	sefesf	f	sefsesf	1
195	2022-04-01	efqf	t	qzdqzd	4
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, user_name, image_url, email, password) FROM stdin;
2	Paul Jean	https://i.pravatar.cc/150?img=68	Paul.j@gmail.com	$2b$08$AByDAu220njE/eTtsQ58h.XT2yKp0BRRsiEq8549P80IXIL6rK/RG
1	Philippe Armand	https://i.pravatar.cc/150?img=17	philippe.a@gmail.com	$2b$08$EQbPwsqugJah7p2l5hDQq..2Cg8uIEWmY1JjyqVuG1qbXdhQJclcG
3	Amaury Durant	https://i.pravatar.cc/150?img=12	amaury.d@gmail.com	$2b$08$pnoSi0645iEdrg8rLbxK7ubSZ1gLrFhqGeaEOcv6DIHv/iZKfhKnC
5	Faustine Lafon	https://i.pravatar.cc/150?img=5	faustine.l@gmail.com	$2b$08$brvq3B9Orf1bboa7cuS8IO2DL4C/kLZjuU3wzZUGAQo3r3e6XGAj6
4	Alison Gosselin	https://i.pravatar.cc/150?img=16	alison.g@gmail.com	$2b$08$3k.bRMHDYlaBykAvMtsvlefziIWJs6eY88/xRH4EAqQescb4pAXMq
\.


--
-- Name: task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.task_id_seq', 195, true);


--
-- Name: task task_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: fki_task_user; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_task_user ON public.task USING btree (user_id);


--
-- Name: task task_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_user FOREIGN KEY (user_id) REFERENCES public."user"(id) NOT VALID;


--
-- PostgreSQL database dump complete
--

