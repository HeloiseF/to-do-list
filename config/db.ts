require("dotenv").config();
import { Pool } from "pg";


const pool = new Pool({
  user: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  host: process.env.PG_HOST,
  database: process.env.PG_DATABASE,
  port: parseInt(`${process.env.PG_PORT}`),
});


export default pool;


