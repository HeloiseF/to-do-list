import styled from "styled-components";

export const SUserList = styled.ul `
    padding: 10px 0px;
    position:absolute;
    z-index: 1;
    background-color: white;
    width: 350px;
    height: 202px;
    left: 0px;
    top: 48px;
    box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.14), 0px 1px 18px rgba(0, 0, 0, 0.12), 0px 3px 5px -1px rgba(0, 0, 0, 0.2);
    border-radius: 3px;
    overflow-y:scroll;
`;

export const SUserListItem = styled.li`
    width:100%;
    height: 62px;
    padding: 10px 20px; 
    background : ${props => props.isSelected === true ? '#F7F8F9': '#FFFFFF'};
    button{
        display: flex;
        width: 100%;
        height: 100%;
        .picture{
            border-radius:50%;
        }
        div{
            margin-left:13px;
            display:flex;
            flex-direction: column;
            span{
                white-space: nowrap
            }
            span:first-child{
                font-weight: 500;
                font-size:16px;
                text-align: left;
            } 
            span:last-child{
                font-weight: 400;
                font-size: 14px;
                color: #909191;
            } 
        }        
    }
`;