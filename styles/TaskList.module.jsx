import styled from "styled-components";

const STaskList = styled.div`

    .completedTasks, .inProgressTasks {
        overflow-y: scroll;
        /* overflow-y:hidden; */
        margin-top: 7px;  
        ul{
            min-height: calc((100vh - 64px - 60px - 60px - (36px * 2) - 14px)/2);
            max-height: calc((100vh - 64px - 60px - 60px - (36px * 2) - 14px)/2);
            .isOver{       
                display: flex; 
                padding: 26px;
                justify-content: space-between;
                align-items: center;
                font-weight: 500;
                font-size: 14px;
        }     
        }

    }
    .completedTasks {
        span,h4 {
                    text-decoration-line: line-through;
                } 

    }

    .title{
        display: flex; 
        padding: 0 26px;
        justify-content: space-between;
        align-items: center;
        height: 36px;
        background: rgba(219, 70, 57, 0.1);
        span{
            font-weight: 500;
            font-size: 14px;
            color: #DC4D3C;
        } 
        
    }


`;


export default STaskList;