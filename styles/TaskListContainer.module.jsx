import styled from "styled-components";

const STaskListContainer = styled.div`
        width: 372px;   
        max-height: calc( 100vh - 64px );

    h3, input {
        width: 360px;
        height: 60px;          
        display:flex;
        justify-content: start;
        align-items: center;
        padding: 0 21px;     
        font-weight: black; 
    } 
    
    input {
        font-size: 16px;
        font-weight: 500;
        border-bottom: 2px #E4E8EB solid;
    }


`;

export default STaskListContainer;