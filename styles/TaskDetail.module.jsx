import styled from "styled-components";


const STaskDetail = styled.section `
    background: #F7F8F9;
    width: calc( 100vw - 372px);
    max-height: calc(100vh - 64px);

    .top{
        display:flex;
        justify-content: space-between;
        align-items: center;
        height: 60px;
        padding: 0 26px;
        background: white;
        border-bottom: 2px #E4E8EB solid;
    }
    button.completed{
        display: flex;
        justify-content: center;
        align-items: center;
        height: 32px;
        width: 202px;
        border: 1px solid #E4E8EB;
        padding: 5px;
        span{
            margin-left: 5px;
            font-size: 12px;
            font-weight: 500;
            color: #9B9C9C;
        }
        img{
            filter: invert(79%) sepia(0%) saturate(212%) hue-rotate(320deg) brightness(78%) contrast(95%);
        }
    }
    div.UserEndDate{
        display:flex;        
        margin: 21px auto 27px 34px;
        position:relative;

    }

    div.buttons{
        display:flex;
        margin: 21px auto 27px 34px;
    }
    .user, .endDate {
        display:flex;
        justify-content: start;
        align-items: center;
        margin-right: 21px;
        background-color: white;
        border-radius: 21px;
        filter: drop-shadow(0px 3px 10px #E4E8EB);
        height: 42px;
        width: 148px;
        &.task{
            width: auto;
            padding-right: 10px;
            span:first-child{
                background: url(${props => props.userPicture}) no-repeat top center;
                background-size: cover;
            }
        }
        font-weight: bold;
        font-size: 12px;
        color: #B7B8B8;
        outline:none;
        span:first-child,div{
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 0 10px;
            height:28px;
            width: 28px;
            border-radius: 50%;
            border: 1px dashed #979797;
            color: #898989;
            font-size: 16px;
            font-weight: 500;
        }
    }
    div.description{
        background: white;
        height:293px;
        width:100%;
        border: 1px solid #E4E8EB;
        div.container{
            display:flex;
            justify-content: start;
            padding-left: 17px;
            padding-top: 17px;
            height:55px;
            width:1169px;
            position:relative;
            span.description{
                display:flex;
                align-items:center;
                padding-left: 14px;
                font-size: 14px;
                font-weight: 500;
            }
        }   
        textarea, p.description {
            display: block;
            padding: 14px;
            height: 164px;
            width: 1094px;
            margin-left: 41px;
            margin-bottom: 14px;
            font-size: 14px;
            font-weight: 400;
            color: #212223;
            border:#E4E8EB 1px solid;
            overflow-y: scroll;
        }     

    }

    .buttonContainer {
        display: flex;
        justify-content: end;
        align-items: center;
        border-top: 1px solid #E4E8EB;
        height: 60px;
        button {
            height: 32px;
            width: 120px;
            border-radius: 3px;
            background-color: #E74C3C;
            color: #FFFFFF;
            font-size: 12px;
            font-weight: 500;
            margin-right: 34px;
        }
        
    }

    input,span.title {
        font-size: 22px;
        font-weight: 400;
    }

    .react-calendar {
        position: absolute;
        padding: 5px;
        width: 360px;
        height: 230px;
        left: 0px;
        top: 48px;
        box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.14), 0px 1px 18px rgba(0, 0, 0, 0.12), 0px 3px 5px -1px rgba(0, 0, 0, 0.2);
        border-radius: 3px;
        z-index: 1;
        background-color: white;
        font-size: 14px;
        button {
            width: 16px;
            font-family: Roboto, sans-serif;
            font-size: 12px;
            padding: 5px;
            &:hover{
                background: #F7F8F9;
                color:black;
                border-radius: 7px;
                &.react-calendar__tile--active {
                    background: rgba(219,70,57);
                    border-radius: 7px;
                    color: white;
                }
            }
        }
        div.react-calendar__navigation {
            margin: 0px;
        }
        .react-calendar__tile--active {
            background: rgba(219,70,57);
            border-radius: 7px;
            color: white;
        }
        .react-calendar__tile--now{
            background: rgba(219,70,57,0.2);
            border-radius: 7px;
        }
    }

    `;

export default STaskDetail;