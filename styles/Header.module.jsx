import styled from "styled-components";

const SHeader = styled.header`
    display: flex;
    justify-content : space-between;
    align-items: center;  
    height: 64px;
    background: #FFFFFF;
    margin-left:10px;
    border-bottom: 2px #E4E8EB solid;
    .logo{
        width: 120px;
        height: 47px;
        position: relative;
    }
    button {
        height: 32px;
        width: 120px;
        border-radius: 3px;
        background-color: #E74C3C;
        color: #FFFFFF;
        font-size: 12px;
        font-weight: 500;
        margin-right: 34px;
    }
`;

export default SHeader;
