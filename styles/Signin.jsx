import styled from 'styled-components';


 const SSigninForm = styled.main`
    min-height: calc(100vh - 64px);
    width: 100vw;
    background: #F7F8F9;
    display: flex;
    justify-content: center;
    align-items: center;
    h2{
        width: 100%;
        font-family : Roboto, sans-serif;
        font-size: 14px;
    }
    p{
        padding: 10px;
        margin: 20px;
        border: 1px solid red;
    }
    div{
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center; 
        background: white;
        width: 400px;
        padding: 20px;
        border-radius: 3px;
        border: solid 1px #E4E8EB;
    }
    form{
        margin: 20px;
        background: white;
        padding: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        width: 350px;
        border: solid 1px #E4E8EB;
        border-radius: 3px;
        label{
            margin: 20px;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            input{
                margin:5px;
                border-radius: 3px;
                border: solid 1px #E4E8EB;
                width: 280px;
                height: 30px;
                padding: 5px;
                padding-left: 15px;
            }
        }
        button{
            margin-top:20px;
            height: 32px;
            width: 120px;
            border-radius: 3px;
            background-color: #E74C3C;
            color: #FFFFFF;
            font-size: 14px;
            font-weight: 500;
        }
    }
 
 `; 
 
 
 
 
 
 
 export default SSigninForm;