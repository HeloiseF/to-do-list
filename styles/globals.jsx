import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
  html,
  body {
    padding: 0;
    margin: 0;
    font-family:  Roboto, sans-serif;
  }

  * {
    box-sizing: border-box;
    padding: 0;
    margin: 0;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  h3 {
    background-color: #E74C3C;
    color: #FFFFFF;
    font-size: 16px;
  }

  input {
    border: none;
    :focus{
      outline:none;
    }
  }

textarea {
    :focus{
      outline:none;
    }
  }

  button {
    background: none;
    border: none;
    &:hover {
      cursor: pointer;
    }
  }

  ul {
    list-style-type: none;   
  }
  

`;
export default GlobalStyles;
