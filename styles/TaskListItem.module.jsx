import styled from "styled-components";

const STaskListItem = styled.div`
    background: ${props => props.isSelected ? '#F7F8F9' : 'white'};
    box-shadow: ${props => props.isSelected ? 'inset 3px 0px 0px #DB4437' : null};
    div.left {
        margin-left: 11px; 
        width: 160px;
    }
    button{
    width:100%;
    height: 80px;
    display: flex;
    padding: 20px 0px 20px 20px ;
    text-align: left;
    background: ${props => props.isDragging ? '#F7F8F9' : 'white'};
    }
    span{
        &.cercle {
            display:block;
            background: url(${props => props.userPicture}) top center;
            background-size: cover;
            height: 40px;
            width: 40px;
            border-radius: 50%;
            border: 1px solid #979797;
            &.completed{
                border: none;        
            }
        }
        &.userName {
            font-size: 13px;
            color: ${props => props.isCompleted ? `#9B9C9C` : `#252627`} ;
        }

        &.endDate {
            width: 80px;
            font-size: 12px;
            color: ${props => props.isCompleted ? `#A5A6A6` : `#A5A6A6`};
        }

    }
    h4{
        color: ${props => props.isCompleted ? `#595C60` : `black`};
        height:50%;
        width: 170px;
        font-size: 14px;
        font-weight: bold;
    }
`;


export default STaskListItem;