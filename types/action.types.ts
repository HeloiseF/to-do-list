//Typescript generics 
//The T allows you to capture the type 
//that is provided at the time of calling 
//the function. Also, the function uses the T type variable as its return type.



export interface Action<T> {
    type: string;
    payload: T;
}

export interface ActionWithoutPayload {
    type: string;
}

