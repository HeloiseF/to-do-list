export interface UserAuth {
    id: number;
    email: string;
    password: string
}


export interface User {
    id: number;
    userName: string;
    email: string;
    userPicture: string
}

export interface Task {
    id: number;
    description: string,
    endDate: Date,
    isCompleted: boolean,
    title: string,
    assignee: {
        id: number,
        userName: string,
        userPicture: string,
    }
}