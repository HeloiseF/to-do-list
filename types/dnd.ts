export const ItemTypes = {
    COMPLETED_TASK: 'completedTask',
    IN_PROGRESS_TASK: 'inProgressTask',
}
