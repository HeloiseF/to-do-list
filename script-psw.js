const bcrypt = require('bcrypt');
require("dotenv").config();
const {Pool} = require('pg')

const pool = new Pool({
  user: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  host: process.env.PG_HOST,
  database: process.env.PG_DATABASE,
  port: parseInt(`${process.env.PG_PORT}`),
});

const jwtRounds = parseInt(process.env.JWT_SALTROUNDS, 10)
const pswds = [
    'password01',
    'password02',
    'password03',
    'password04',
    'password05',
  ];

for (let i = 0; i < pswds.length; i += 1) {
    const pswd = bcrypt.hashSync(pswds[i], jwtRounds);
    const id = i + 1;
    pool.query(`UPDATE public.user SET password = $1 WHERE id = $2`, [pswd, id]);
}