import { useRouter } from "next/router";
import { useSession } from 'next-auth/react';
import { useEffect } from 'react';


function Home(): JSX.Element {
  const { data: session } = useSession();
  const router = useRouter();
  useEffect(() => {
    if (session === undefined || session === null) {
      router.replace('/signin');
    }
    else {
      router.replace('/toDoList');
    }
  }, [session])

  return null;
}



export default Home;