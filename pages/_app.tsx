import GlobalStyles from '../styles/globals';
import Header from '../components/header';
import { SessionProvider } from 'next-auth/react'
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { persistor } from '../redux/store';
import { store } from '../redux/store';
import type { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <SessionProvider session={pageProps.session}>
        <PersistGate loading={null} persistor={persistor} >
          <GlobalStyles />
          <Header />
          <Component {...pageProps} />
        </PersistGate>
      </SessionProvider>
    </Provider>

  )

}

export default MyApp;
