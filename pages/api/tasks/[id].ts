import prisma from '../../../prisma';
import type { NextApiRequest, NextApiResponse } from 'next';

const taskById = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === "DELETE") {
    try {
      const id = req.query.id as string;
      const taskId = parseInt(id);
      await prisma.task.delete({
        where: {
          id: taskId
        }
      })
      res.status(200);
      res.end();
    }
    catch (err) {
      res.status(400).send(err);
    }
  }
}

export default taskById;