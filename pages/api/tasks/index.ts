import type { NextApiRequest, NextApiResponse } from 'next';
import { Update } from "../../../redux/taskList/taskList.utils";
import prisma from '../../../prisma';


const task = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === "POST") {
    try {
      const { title, assignee, id, endDate, description, isCompleted }: Update = req.body;

      let newDate = new Date(new Date(endDate).toLocaleString('fr-FR', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      }).split('/').reverse().join('-'));

      await prisma.task.upsert({
        where: {
          id: id
        },
        update: {
          description,
          isCompleted,
          endDate: newDate,
          title,
          assigneeId: assignee.id
        },
        create: {
          description,
          isCompleted,
          endDate: newDate,
          title,
          assigneeId: assignee.id
        }
      })
      res.status(200);
      res.end();
    } catch (err) {
      console.log(err)
      res.status(400).send(err);
    }
  }
}

export default task;


