import type { NextApiRequest, NextApiResponse } from 'next';
import prisma from '../../../../prisma';

const userById = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === "GET") {
    try {
      const id = req.query.id as string;
      const userId = parseInt(id);
      const user = await prisma.user.findUnique({
        where: {
          id: userId,
        }
      });
      res.status(200).json(user);
      res.end();
    }
    catch (err) {
      res.status(400).send(err);
    }
  }
}

export default userById;