import type { NextApiRequest, NextApiResponse } from 'next';
import prisma from '../../../../prisma';


const tasksByUser = async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === 'GET') {
        try {
            const id = req.query.id as string;
            const assigneeId = parseInt(id);
            const tasks = await prisma.task.findMany({
                where: {
                    assigneeId: assigneeId
                },
                select: {
                    id: true,
                    description: true,
                    endDate: true,
                    isCompleted: true,
                    title: true,
                    assignee: {
                        select: {
                            id: true,
                            userName: true,
                            userPicture: true,

                        }
                    }
                }
            })
            res.status(200).json(tasks)
        }
        catch (err) {
            res.status(400)
        }
    }
}



export default tasksByUser;