import type { NextApiRequest, NextApiResponse } from 'next';

import prisma from '../../../prisma';
import { User } from '../../../types/dbcontent.types';

const users = async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === 'GET') {
        try {
            const users: User[] | [] = await prisma.user.findMany(
                {
                    select: {
                        password: false,
                        id: true,
                        userName: true,
                        userPicture: true,
                        email: true
                    },
                    orderBy: {
                        userName: 'asc'
                    }
                }
            );
            res.status(200).json(users);
        }
        catch (err) {
            res.status(400).send(err);
        }
    }
}

export default users;