import type { NextApiRequest, NextApiResponse } from 'next';
import { UserAuth } from '../../../types/dbcontent.types';
import { compare } from "bcrypt";
import prisma from '../../../prisma';

const user = async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === "POST") {
        try {
            const { email, password }: { email: string, password: string } = req.body;
            const loggedUser: UserAuth | undefined = await prisma.user.findUnique({
                where: {
                    email: email
                }
            });
            if (loggedUser) {
                compare(password, loggedUser.password, (err, result) => {
                    if (!err && result) {
                        return res.json(loggedUser);
                    } else {
                        return res.json(null);
                    }
                })
            } else {
                return res.send(null);
            }
        }
        catch (err) {
            res.status(400).json(err);
        }
    }
}

export default user;