import NextAuth from "next-auth/next";
import CredentialsProvider from "next-auth/providers/credentials";
import { UserAuth } from "../../../types/dbcontent.types";
import axios from "axios";
require('dotenv').config()


export default NextAuth({
    providers: [
        CredentialsProvider({
            name: "credentials",
            credentials: {
                email: {
                    label: "Email",
                    type: "email",
                },
                password: { label: "Password", type: "password" },
            },
            authorize: async (credentials) => {
                const res = await axios.post(`${process.env.NEXT_PUBLIC_SERVER}/api/auth/user`, credentials);
                const user: UserAuth | undefined = res.data;
                if (user) {
                    return { ...user, id: user.id.toString() };
                } else {
                    return null;
                }

            }
        }),
    ],
    pages: {
        signIn: '/signin',
    },
    callbacks: {
        jwt: ({ token, user }) => {
            if (user) {
                token.id = user.id
            }
            return token
        },
        session: ({ session, token }) => {
            if (token) {
                session.id = token.id
            }
            return session
        }
    },
    jwt: {
        secret: process.env.NEXTAUTH_SECRET,
    },
    session: {
        strategy: "jwt",
    },

})
