import { PrismaClient, Prisma } from '@prisma/client'

const prisma = new PrismaClient()

async function seed() {
    await prisma.user.createMany({
        data: [{
            id: 4,
            userName: "Alison Gosselin",
            userPicture: "https://i.pravatar.cc/150?img=16",
            email: "alison.g@gmail.com",
            password: "$2b$08$3k.bRMHDYlaBykAvMtsvlefziIWJs6eY88/xRH4EAqQescb4pAXMq"
        },
        {
            id: 3,
            userName: "Amaury Durant",
            userPicture: "https://i.pravatar.cc/150?img=12",
            email: "amaury.d@gmail.com",
            password: '$2b$08$pnoSi0645iEdrg8rLbxK7ubSZ1gLrFhqGeaEOcv6DIHv/iZKfhKnC'
        },
        {
            id: 5,
            userName: "Faustine Lafon",
            userPicture: "https://i.pravatar.cc/150?img=5",
            email: "faustine.l@gmail.com",
            password: "$2b$08$brvq3B9Orf1bboa7cuS8IO2DL4C/kLZjuU3wzZUGAQo3r3e6XGAj6"
        },
        {
            id: 2,
            userName: "Paul Jean",
            userPicture: "https://i.pravatar.cc/150?img=68",
            email: "Paul.j@gmail.com",
            password: "$2b$08$AByDAu220njE/eTtsQ58h.XT2yKp0BRRsiEq8549P80IXIL6rK/RG"
        },
        {
            id: 1,
            userName: "Philippe Armand",
            userPicture: "https://i.pravatar.cc/150?img=17",
            email: "philippe.a@gmail.com",
            password: "$2b$08$EQbPwsqugJah7p2l5hDQq..2Cg8uIEWmY1JjyqVuG1qbXdhQJclcG"
        }]
    })
}

seed()
    .catch((e) => {
        console.error(e)
        process.exit(1)
    })
    .finally(async () => {
        await prisma.$disconnect()
    })
